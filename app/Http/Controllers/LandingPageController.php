<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    protected $locale;

    public function __construct()
    {

        $this->locale = app()->getLocale();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::where('featured', true)->take(8)->inRandomOrder()->get();

        // return view('landing-page')->with('products', $products);
        return view('traditional.index');
    }

    public function contact()
    {
        
        return view('traditional.contact');
    }

    public function wishlist()
    {

        return view('traditional.shop.wishlist');
    }

    public function about()
    {

        return view('traditional.about');
    }
}
