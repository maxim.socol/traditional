<header class="">
    <div class="header-area transparent-bar header-2 hm-4-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-4 d-none d-sm-block d-md-block">
                    <div class="language-currency">
                        <div class="menu-icon">
                            <button><i class="ion-android-menu"></i></button>
                        </div>
                        <div class="language">
                            <ul class="select-language">
                                <li>
                                    @if(array_key_exists(app()->getLocale(), config('translatable.locales')))
                                        <a>{{ config('translatable.locales')[app()->getLocale()] }}<i class="ion-ios-arrow-down"></i></a>
                                        <ul>
                                            @foreach(config('translatable.locales') as $key => $locales)
                                            <li>
                                                @if($key == app()->getLocale()) @continue @endif
                                                <a href="{{ route('locale.switch', $key) }}">{{ $locales }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="menu-icon menu-icon-none">
                        <button><i class="ion-android-menu"></i></button>
                    </div>
                </div>
                <div class="col-lg-7 col-md-5 col-5">
                    <div class="logo-menu-wrapper text-center">
                        <div class="logo pr-155">
                            <a href="{{ route('index') }}"><img src="{{asset('assets/img/logo/logo.png')}}" alt="" /></a>
                        </div>
                        <div class="sticky-logo pr-155">
                            <a href="{{ route('index') }}"><img src="{{asset('assets/img/logo/2.png')}}" alt="" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-7">
                    <div class="header-site-icon">
                        <div class="header-search same-style">
                            <button class="sidebar-trigger-search">
                                <span class="ti-search"></span>
                            </button>
                        </div>
                        <div class="header-login same-style">
                            <a href="login-register.html">
                                <span class="ti-user"></span>
                            </a>
                        </div>
                        <div class="header-cart same-style">
                            <button class="sidebar-trigger">
                                <i class="ti-shopping-cart"></i>
                                <span class="count-style">03</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mobile-menu-area col-12">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul class="menu-overflow">
                                <li><a href="{{ route('index') }}">Pagina principala</a></li>
                                <li><a href="{{ route('about') }}">Despre noi</a></li>
                                <li><a href="{{ route('shop') }}">Shop</a></li>
                                <li><a href="{{ route('contact') }}">Contact us</a></li>
                            </ul>
                        </nav>							
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="header-height"></div>
<!-- menu-style start -->
<div class="sidebarmenu-wrapper">
    <div class="menu-close">
        <button><i class="ion-android-close"></i></button>
    </div>
    <div class="sidebarmenu-style">
        <nav class="menu">
            <ul>
                <li><a href="{{ route('index') }}">Pagina principala</a></li>
                <li><a href="{{ route('about') }}">Despre noi</a></li>
                <li><a href="{{ route('shop') }}">Shop</a></li>
                <li><a href="{{ route('contact') }}">Contact us</a></li>
            </ul>
        </nav>
    </div>
    <div class="newsletter-area">
        <h4 class="newsletter-title">Newsletter</h4>
        <p>Send us your mail or next updates.</p>
        <div id="mc_embed_signup" class="subscribe-form">
            <form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll" class="mc-form">
                    <input type="email" value="" name="EMAIL" class="email" placeholder="Your Mail :" required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div class="mc-news" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
        </div>
    </div>
    <div class="follow-area mt-60">
        <h4 class="newsletter-title">Follow Us</h4>
        <div class="follow-icon">
            <ul>
                <li class="facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
                <li class="twitter"><a href="#"><i class="ion-social-twitter"></i></a></li>
                <li class="instagram"><a href="#"><i class="ion-social-instagram"></i></a></li>
                <li class="tumblr"><a href="#"><i class="ion-social-tumblr"></i></a></li>
            </ul>
        </div>
    </div>
</div>