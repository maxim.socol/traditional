<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Traditional</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('traditional.layouts.head')

    </head>
    <body>
        <div class="wrapper">
            <!-- header start -->
            @if(request()->segment(2) == '')
                @include('traditional.layouts.menu')
            @else
                @include('traditional.layouts.menu_page')
            @endif
            <!-- sidebar-cart start -->
            @include('traditional.components.cart')
            <!-- main-search start -->
            @include('traditional.components.search')
            
            @yield('content')

            @include('traditional.layouts.footer')
            <!-- modal -->
            @include('traditional.components.modal')
        </div>
        
		<!-- all js here -->
        <script src="{{asset('assets/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('assets/js/popper.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
        <script src="{{asset('assets/js/ajax-mail.js')}}"></script>
        <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>

        @yield('scripts')

    </body>
</html>
