@extends('traditional.layouts.app')

@section('content')

    <div class="breadcrumb-area mt-37 hm-4-padding">
        <div class="container-fluid">
            <div class="breadcrumb-content text-center border-top-2">
                <h2>About Us</h2>
                <ul>
                    <li>
                        <a href="#">home</a>
                    </li>
                    <li> about us </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="banner-area hm-4-padding">
        <div class="container-fluid">
            <div class="banner-img">
                <a href="#"><img src="{{asset('assets/img/banner/16.jpg')}}" alt=""></a>
            </div>
        </div>
    </div>
    <div class="about-us-area hm-3-padding pt-125 pb-125">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-us-title">
                        <h3>We are HandMade store. <br>Modern HandMade eCommerce Template </h3>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-us-details">
                        <p class="about-us-pera-mb">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labor et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess cill dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa offici deserunt mollit anim id est laborum. </p>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudant totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae di sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-area hm-3-padding h3-services pb-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-services orange mb-30">
                        <div class="services-icon">
                            <img alt="" src="{{asset('assets/img/icon-img/3.png')}}">
                        </div>
                        <div class="services-text">
                            <h5>FREE SHIPPING</h5>
                            <p>Free shipping on all order</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-services yellow mb-30">
                        <div class="services-icon">
                            <img alt="" src="{{asset('assets/img/icon-img/4.png')}}">
                        </div>
                        <div class="services-text">
                            <h5>ONLINE SUPPORT</h5>
                            <p>Online support 24 hours a day</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-services purple mb-30">
                        <div class="services-icon">
                            <img alt="" src="{{asset('assets/img/icon-img/5.png')}}">
                        </div>
                        <div class="services-text">
                            <h5>MONEY RETURN</h5>
                            <p>Back guarantee under 5 days</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-services sky mb-30">
                        <div class="services-icon">
                            <img alt="" src="{{asset('assets/img/icon-img/6.png')}}">
                        </div>
                        <div class="services-text">
                            <h5>MEMBER DISCOUNT</h5>
                            <p>Onevery order over $150</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonials-area">
        <div class="container-fluid">
            <div class="testimonial-active owl-carousel pt-130 pb-125 gray-bg">
                <div class="single-testimonial text-center">
                    <img alt="" src="{{asset('assets/img/team/1.png')}}">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat</p>
                    <div class="testimonial-icon">
                        <i class="ti-direction-alt"></i>
                    </div>
                    <h4>Shakara Tasnim </h4>
                    <span>Customer</span>
                </div>
                <div class="single-testimonial text-center">
                    <img alt="" src="{{asset('assets/img/team/1.png')}}">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat</p>
                    <div class="testimonial-icon">
                        <i class="ti-direction-alt"></i>
                    </div>
                    <h4>Farhana Shuvo </h4>
                    <span>Customer</span>
                </div>
                <div class="single-testimonial text-center">
                    <img alt="" src="{{asset('assets/img/team/1.png')}}">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat</p>
                    <div class="testimonial-icon">
                        <i class="ti-direction-alt"></i>
                    </div>
                    <h4>Samia Robiul </h4>
                    <span>Customer</span>
                </div>
            </div>
        </div>
    </div>
    <div class="team-area hm-3-padding pt-130 pb-95">
        <div class="container-fluid">
            <div class="section-title-2 text-center mb-55">
                <h2 class="mb-12">Team Member</h2>
                <p>There are many variations of passages of Lorem Ipsum. </p>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="team-wrapper mb-30">
                        <div class="team-img">
                            <img src="{{asset('assets/img/team/1.jpg')}}" alt="">
                        </div>
                        <div class="team-content">
                            <h4>Zoe Jones</h4>
                            <span>Founder </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="team-wrapper mb-30">
                        <div class="team-img">
                            <img src="{{asset('assets/img/team/2.jpg')}}" alt="">
                        </div>
                        <div class="team-content">
                            <h4>Sienna Nguyen</h4>
                            <span>Manager </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="team-wrapper mb-30">
                        <div class="team-img">
                            <img src="{{asset('assets/img/team/3.jpg')}}" alt="">
                        </div>
                        <div class="team-content">
                            <h4>Liam Ryan</h4>
                            <span>Director </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="team-wrapper mb-30">
                        <div class="team-img">
                            <img src="{{asset('assets/img/team/4.jpg')}}" alt="">
                        </div>
                        <div class="team-content">
                            <h4>Charlotte Taylor</h4>
                            <span>Chairmen </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('traditional.components.brands')

@endsection
