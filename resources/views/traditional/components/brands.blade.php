<div class="brand-logo-area hm-4-padding">
    <div class="container-fluid">
        <div class="brand-logo-active owl-carousel gray-bg ptb-130">
            <div class="single-logo">
                <img alt="" src="{{asset('assets/img/brand-logo/1.png')}}">
            </div>
            <div class="single-logo">
                <img alt="" src="{{asset('assets/img/brand-logo/2.png')}}">
            </div>
            <div class="single-logo">
                <img alt="" src="{{asset('assets/img/brand-logo/3.png')}}">
            </div>
            <div class="single-logo">
                <img alt="" src="{{asset('assets/img/brand-logo/4.png')}}">
            </div>
            <div class="single-logo">
                <img alt="" src="{{asset('assets/img/brand-logo/5.png')}}">
            </div>
            <div class="single-logo">
                <img alt="" src="{{asset('assets/img/brand-logo/3.png')}}">
            </div>
        </div>
    </div>
</div>