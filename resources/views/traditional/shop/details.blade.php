@extends('traditional.layouts.app')

@section('content')
    
    <div class="breadcrumb-area mt-37 hm-4-padding">
        <div class="container-fluid">
            <div class="breadcrumb-content text-center border-top-2">
                <h2>product details</h2>
                <ul>
                    <li>
                        <a href="#">home</a>
                    </li>
                    <li>product details</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="product-details-area hm-3-padding ptb-130">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-details-img-content">
                        <div class="product-details-tab mr-40">
                            <div class="product-details-large tab-content">
                                <div class="tab-pane active" id="pro-details1">
                                    <div class="easyzoom easyzoom--overlay">
                                        <a href="{{asset('assets/img/product-details/bl1.jpg')}}">
                                            <img src="{{asset('assets/img/product-details/l1.jpg')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pro-details2">
                                    <div class="easyzoom easyzoom--overlay">
                                        <a href="{{asset('assets/img/product-details/bl2.jpg')}}">
                                            <img src="{{asset('assets/img/product-details/l2.jpg')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pro-details3">
                                    <div class="easyzoom easyzoom--overlay">
                                        <a href="{{asset('assets/img/product-details/bl3.jpg')}}">
                                            <img src="{{asset('assets/img/product-details/l3.jpg')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pro-details4">
                                    <div class="easyzoom easyzoom--overlay">
                                        <a href="{{asset('assets/img/product-details/bl4')}}.jpg">
                                            <img src="{{asset('assets/img/product-details/l4.jpg')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pro-details5">
                                    <div class="easyzoom easyzoom--overlay">
                                        <a href="{{asset('assets/img/product-details/bl3.jpg')}}">
                                            <img src="{{asset('assets/img/product-details/l3.jpg')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-details-small nav mt-12 product-dec-slider owl-carousel">
                                <a class="active" href="#pro-details1">
                                    <img src="{{asset('assets/img/product-details/s1.jpg')}}" alt="">
                                </a>
                                <a href="#pro-details2">
                                    <img src="{{asset('assets/img/product-details/s2.jpg')}}" alt="">
                                </a>
                                <a href="#pro-details3">
                                    <img src="{{asset('assets/img/product-details/s3.jpg')}}" alt="">
                                </a>
                                <a href="#pro-details4">
                                    <img src="{{asset('assets/img/product-details/s4.jpg')}}" alt="">
                                </a>
                                <a href="#pro-details5">
                                    <img src="{{asset('assets/img/product-details/s3.jpg')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product-details-content">
                        <h2>Awesome Bracelet </h2>
                        <div class="product-rating">
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <span> ( 01 Customer Review )</span>
                        </div>
                        <div class="product-price">
                            <span class="old">$22.00 </span>
                            <span>$19.00</span>
                        </div>
                        <div class="product-overview">
                            <h5 class="pd-sub-title">Product Overview</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat consectetur adipic.</p>
                        </div>
                        <div class="product-size">
                            <h5 class="pd-sub-title">Product size</h5>
                            <ul>
                                <li>
                                    <a href="#">s</a>
                                </li>
                                <li>
                                    <a href="#">m</a>
                                </li>
                                <li>
                                    <a href="#">l</a>
                                </li>
                                <li>
                                    <a href="#">xl</a>
                                </li>
                                <li>
                                    <a href="#">lm</a>
                                </li>
                            </ul>
                        </div>
                        <div class="product-color">
                            <h5 class="pd-sub-title">Product color</h5>
                            <ul>
                                <li class="red">b</li>
                                <li class="pink">p</li>
                                <li class="blue">b</li>
                                <li class="sky2">b</li>
                                <li class="green">y</li>
                                <li class="purple2">g</li>
                            </ul>
                        </div>
                        <div class="quickview-plus-minus">
                            <div class="cart-plus-minus">
                                <input type="text" value="02" name="qtybutton" class="cart-plus-minus-box">
                            </div>
                            <div class="quickview-btn-cart">
                                <a class="btn-style cr-btn" href="#"><span>add to cart</span></a>
                            </div>
                            <div class="quickview-btn-wishlist">
                                <a class="btn-hover cr-btn" href="#"><span><i class="ion-ios-heart-outline"></i></span></a>
                            </div>
                        </div>
                        <div class="product-categories">
                            <h5 class="pd-sub-title">Categories</h5>
                            <ul>
                                <li>
                                    <a href="#">fashion ,</a>
                                </li>
                                <li>
                                    <a href="#">electronics ,</a>
                                </li>
                                <li>
                                    <a href="#">toys ,</a>
                                </li>
                                <li>
                                    <a href="#">food ,</a>
                                </li>
                                <li>
                                    <a href="#">jewellery </a>
                                </li>
                            </ul>
                        </div>
                        <div class="product-share">
                            <h5 class="pd-sub-title">Share</h5>
                            <ul>
                                <li>
                                    <a href="#"><i class="ion-social-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="ion-social-tumblr"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="ion-social-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"> <i class="ion-social-instagram-outline"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @include('traditional.components.brands')

@endsection