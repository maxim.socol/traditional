@extends('traditional.layouts.app')

@section('content')


    <div class="breadcrumb-area mt-37 hm-4-padding">
        <div class="container-fluid">
            <div class="breadcrumb-content text-center border-top-2">
                <h2>Cart page</h2>
                <ul>
                    <li>
                        <a href="#">home</a>
                    </li>
                    <li>Cart page</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="banner-area hm-4-padding">
        <div class="container-fluid">
            <div class="banner-img">
                <a href="#"><img src="assets/img/banner/16.jpg" alt=""></a>
            </div>
        </div>
    </div>
    <div class="product-cart-area hm-3-padding pt-120 pb-130">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="table-content table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product-name">products</th>
                                    <th class="product-price">products name</th>
                                    <th class="product-name">price</th>
                                    <th class="product-price">quantity</th>
                                    <th class="product-quantity">total</th>
                                    <th class="product-subtotal">delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#"><img src="{{asset('assets/img/cart/4.jpg')}}" alt=""></a>
                                    </td>
                                    <td class="product-name">
                                        <a href="#">Ptttery Tea Mug</a>
                                    </td>
                                    <td class="product-price"><span class="amount">$128.00</span></td>
                                    <td class="product-quantity">
                                        <div class="quantity-range">
                                            <input class="input-text qty text" type="number" step="1" min="0" value="1" title="Qty" size="4">
                                        </div>
                                    </td>
                                    <td class="product-subtotal">$128.00</td>
                                    <td class="product-cart-icon product-subtotal"><a href="#"><i class="ion-ios-trash-outline" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#"><img src="{{asset('assets/img/cart/5.jpg')}}" alt=""></a>
                                    </td>
                                    <td class="product-name">
                                        <a href="#">Pottery Showpiece</a>
                                    </td>
                                    <td class="product-price"><span class="amount">$150.00</span></td>
                                    <td class="product-quantity">
                                        <div class="quantity-range">
                                            <input class="input-text qty text" type="number" step="1" min="0" value="1" title="Qty" size="4">
                                        </div>
                                    </td>
                                    <td class="product-subtotal">$150.00</td>
                                    <td class="product-cart-icon product-subtotal"><a href="#"><i class="ion-ios-trash-outline" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#"><img src="{{asset('assets/img/cart/6.jpg')}}" alt=""></a>
                                    </td>
                                    <td class="product-name">
                                        <a href="#">Pottery Water Jug</a>
                                    </td>
                                    <td class="product-price"><span class="amount">$100.00</span></td>
                                    <td class="product-quantity">
                                        <div class="quantity-range">
                                            <input class="input-text qty text" type="number" step="1" min="0" value="1" title="Qty" size="4">
                                        </div>
                                    </td>
                                    <td class="product-subtotal">$100.00</td>
                                    <td class="product-cart-icon product-subtotal"><a href="#"><i class="ion-ios-trash-outline" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#"><img src="{{asset('assets/img/cart/7.jpg')}}" alt=""></a>
                                    </td>
                                    <td class="product-name">
                                        <a href="#">Pottery Flower Vase</a>
                                    </td>
                                    <td class="product-price"><span class="amount">$190.00</span></td>
                                    <td class="product-quantity">
                                        <div class="quantity-range">
                                            <input class="input-text qty text" type="number" step="1" min="0" value="1" title="Qty" size="4">
                                        </div>
                                    </td>
                                    <td class="product-subtotal">$190.00</td>
                                    <td class="product-cart-icon product-subtotal"><a href="#"><i class="ion-ios-trash-outline" aria-hidden="true"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="cart-shiping-update">
                        <div class="cart-shipping">
                            <a class="btn-style cr-btn" href="#">
                                <span>continue shopping</span>
                            </a>
                        </div>
                        <div class="update-checkout-cart">
                            <div class="update-cart">
                                <button class="btn-style cr-btn"><span>update</span></button>
                            </div>
                            <div class="update-cart">
                                <a class="btn-style cr-btn" href="#">
                                    <span>checkout</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-6">
                    <div class="discount-code">
                        <h4>enter your discount code</h4>
                        <div class="coupon">
                            <input type="text">
                            <input class="cart-submit" type="submit" value="enter">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <div class="shop-total">
                        <h3>cart total</h3>
                        <ul>
                            <li>
                                sub total
                                <span>$909.00</span>
                            </li>
                            <li>
                                tax
                                <span>$9.00</span>
                            </li>
                            <li class="order-total">
                                shipping
                                <span>0</span>
                            </li>
                            <li>
                                order total
                                <span>$918.00</span>
                            </li>
                        </ul>
                    </div>
                    <div class="cart-btn text-center mb-15">
                        <a href="#">payment</a>
                    </div>
                    <div class="continue-shopping-btn text-center">
                        <a href="#">continue shopping</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('traditional.components.brands')

@endsection